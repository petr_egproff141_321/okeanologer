﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Okeanologer.Models
{
    public class MeasuresGroups
    {
        public int MesuresGroupID { get; set; }
        public int DateCoordsID { get; set; }
        public int MeasuresID { get; set; }
    }
}