﻿using Okeanologer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Okeanologer.Controllers
{
    public class HomeController : Controller
    {
        readonly ObservationContext db = new ObservationContext();
        public ActionResult Index()
        {
            IEnumerable<Measures> measures = db.Measures;
            IEnumerable<MeasuresGroups> measuresGroups = db.MeasuresGroups;
            IEnumerable<DateCoords> dateCoords = db.DateCoords;
            List<int> someList = new List<int>();
            ViewBag.list = someList;
            return View();
        }

       
    }
}