﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Okeanologer.Models
{
    public class DateCoords
    {
        public int DateCoordsID { get; set; }
        public double Year { get; set; }
        public double Month { get; set; }
        public double Day { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}