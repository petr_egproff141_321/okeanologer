﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Okeanologer.Models
{
    public class ObservationContext : DbContext
    {
        public DbSet<Measures> Measures { get; set; }
        public DbSet<MeasuresGroups> MeasuresGroups { get; set; }
        public DbSet<DateCoords> DateCoords { get; set; }
    }
}