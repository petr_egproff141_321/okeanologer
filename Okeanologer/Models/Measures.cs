﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Okeanologer.Models
{
    public class Measures
    {
        public int MeasuresID { get; set; }
        public double Depth { get; set; }
        public double Temperatur { get; set; }
        public double Salinity { get; set; }
        public double Density { get; set; }
    }
}